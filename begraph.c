
/****************************************************************************/
/* DSA node program example  D.F. ROSS                                      */
/****************************************************************************/

/****************************************************************************/
/* include files and  global data objects                                   */
/****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

/****************************************************************************/
/* define types & constants                                                 */
/****************************************************************************/
#define MAXNOD 20
#define INFIN  9999
#define NULLREF NULL

/****************************************************************************/
/* node element definition (this is hidden!)                                */
/****************************************************************************/
typedef struct nodeelem * noderef;

typedef struct nodeelem {
        char       nname;
        int        ninfo;
        noderef    edges;
        noderef    nodes;
        } nodeelem;

/****************************************************************************/
/* define graph instance                                                    */
/****************************************************************************/
static noderef G     = (noderef) NULLREF;       /* adjacency list           */
static int     adjmat[MAXNOD][MAXNOD];          /* adjacency matrix         */

/****************************************************************************/
/* define display structures: Dijkstra-SPT, Floyd, Warshall, Prim           */
/****************************************************************************/
static int  D[MAXNOD];                          /* Dijkstra-SPT distance    */
static char E[MAXNOD];                          /* Dijkstra-SPT edge        */
static int  L[MAXNOD];                          /* Dijkstra-SPT weight      */

static int  Floyd[MAXNOD][MAXNOD];              /* Floyd matrix             */
static int  Warshall[MAXNOD][MAXNOD];           /* Warshall matrix          */

static int  lowcost[MAXNOD];                    /* Prim lowcost             */
static int  closest[MAXNOD];                    /* Prim closest             */

/****************************************************************************/
/* private operations on the node - basic operationa                        */
/****************************************************************************/
static int      is_empty(noderef N)             { return N == NULLREF; }

static char     get_nname(noderef N)            { return N->nname; }
static int      get_ninfo(noderef N)            { return N->ninfo; }
static noderef  get_edges(noderef N)            { return N->edges; }
static noderef  get_nodes(noderef N)            { return N->nodes; }

static noderef  set_nname(noderef N, char c)    { N->nname = c; return N; }
static noderef  set_ninfo(noderef N, int v)     { N->ninfo = v; return N; }
static noderef  set_edges(noderef N, noderef E) { N->edges = E; return N; }
static noderef  set_nodes(noderef N, noderef M) { N->nodes = M; return N; }

/****************************************************************************/
/* create and initialise an element in the node                             */
/****************************************************************************/
static noderef create_n(char c, int w)
{
   return set_nodes(
             set_edges(
                set_ninfo(
                   set_nname(malloc(sizeof(nodeelem)), c),
                w),
             NULLREF),
          NULLREF);
}

/****************************************************************************/
/* private operations on the graph - basic operations                       */
/****************************************************************************/
/****************************************************************************/
/* head and tail - a RECURSIVE view of the sequence                         */
/****************************************************************************/
static noderef nhead(noderef N)     { return N; }
static noderef ntail(noderef N)     { return get_nodes(N); }

static noderef ehead(noderef E)     { return E; }
static noderef etail(noderef E)     { return get_edges(E); }

/****************************************************************************/
/* CONStruct a new node with the element at the head of the node            */
/****************************************************************************/
static noderef ncons(noderef e, noderef N) { return set_nodes(e, N); }
static noderef econs(noderef e, noderef E) { return set_edges(e, E); }

/****************************************************************************/
/* display the edges                                                        */
/****************************************************************************/
static void b_edisp(noderef E) {
  if (!is_empty(E)) { 
     printf(" %c(%2d)", get_nname(ehead(E)), get_ninfo(ehead(E)));
     b_edisp(etail(E));
     }
}

/****************************************************************************/
/* display the nodes                                                        */
/****************************************************************************/
static void b_ndisp(noderef G) {
  if (is_empty(G)) printf("\n");
  else { printf(" %c : ", get_nname(nhead(G)));
         b_edisp(get_edges(G)); printf("\n");
         b_ndisp(ntail(G));
         }
}

/****************************************************************************/
/* ADD to the node in ascending order                                       */
/****************************************************************************/
static noderef b_addn(char c, noderef G)
{
   if(is_empty(G)){
      //Empty, just add
      return create_n(c, 0);
   }else{
      if(c > get_nname(G)){
         //Greater than
         return ncons(G, b_addn(c, ntail(G))); //gå ner
      }else{
         //Less or equal add
         return ncons(create_n(c, 0), G); //skapa här
      }
   }
}

/****************************************************************************/
/* ADD to the edge in ascending order                                       */
/****************************************************************************/
static noderef b_adde(char c, int w, noderef E)
{
   if(is_empty(E)){
      //Empty, just add
      return create_n(c, w); //NULL
   }else{
      if(c > get_nname(E)){
         //Greater than
         return econs(E, b_adde(c, w, etail(E))); //gå ner
      }else{
         //Less or equal add
         return econs(create_n(c, w), E); //skapa här
      }
   }
}

/****************************************************************************/
/* REMove a  node from the graph                                            */
/****************************************************************************/
static noderef b_remn(char c, noderef G) {
   return get_nname(G) == c ? ntail(G) : ncons(G, b_remn(c, ntail(G)));
   /*if(c == get_nname(G)){
      return ntail(G);
   }else{
      return ncons(G, b_remn(c, ntail(G)));
   }*/
}

/****************************************************************************/
/* REMove an edge from the graph                                            */
/****************************************************************************/
static noderef b_reme(char c, noderef E) {
   if(is_empty(E)){
      return E;
   }else if(c == get_nname(E)){
      return etail(E);
   }else{
      return econs(E, b_reme(c, etail(E)));
   }
}

/****************************************************************************/
/* REMove all edges for a given node from the graph                         */
/****************************************************************************/
static void b_remalle(char c, noderef G) {
   if(!is_empty(G)){
      b_reme(c,ehead(G));
      b_remalle(c,ntail(G));
   }
}

/****************************************************************************/
/* FIND a  node in the graph                                                */
/****************************************************************************/
static noderef b_findn(char c, noderef G) {
   return is_empty(G) ? NULLREF : (get_nname(G) == c ? G : b_findn(c, ntail(G)));
   /*if(!is_empty(G)){
      if(get_nname(G) == c){
         //Found
         return G;
      }else{
         //Search further
         b_findn(c, ntail(G));
      }
   }else{
      //Empty
      return NULLREF;
   }*/
}

/****************************************************************************/
/* FIND an edge in the graph                                                */
/****************************************************************************/
static noderef b_finde(char c, noderef E) {
   return is_empty(E) ? NULLREF : (get_nname(E) == c ? E : b_finde(c, etail(E)));
   /*if(!is_empty(E)){
      if(get_nname(E) == c){
         //Found
         return E;
      }else{
         //Search further
         b_finde(c, etail(E));
      }
   }else{
      //Empty
      return NULLREF;
   }*/
}

/****************************************************************************/
/* FIND the number of nodes in the graph (cardinality nodes)                */
/****************************************************************************/
static int b_nsize(noderef G) {
   return is_empty(G) ? 0 : b_nsize(ntail(G)) + 1;
}

/****************************************************************************/
/* FIND the number of edges in the graph (cardinality edges)                */
/****************************************************************************/
static int b_nedges(noderef E){ //Horisontellt
   return is_empty(E) ? -1 : b_nedges(etail(E)) + 1;
}

static int b_esize(noderef G){ //Vertikalt
   return is_empty(G) ? 0 : b_nedges(G) + b_esize(ntail(G));
}

/****************************************************************************/
/* CREATE the adjacency matrix (AM)                                         */
/****************************************************************************/
/****************************************************************************/
/* Find the position (index) of a node in the adjacency list (node list)    */
/* NB: this is used to determine the index for the edge                     */
/*     this index is in turn used to fill in the weight in the AM           */
/* e.g. for an adjacency list:                                              */
/*               (position 0)   a => b(3) -> c(2) eol                       */
/*               (position 1)   b => a(3) -> c(7) eol                       */
/*               (position 2)   c => a(2) -> b(7) eol                       */
/*                              eol                                         */
/* get_pos("b") will give 1 (and hence AM[0][1] is set to 3 i.e. a-3-b)     */
/****************************************************************************/
static int get_pos(noderef fnode){
   int count = 0;
   noderef tmp = G;

   while(get_nname(fnode) != get_nname(tmp)){
      tmp = ntail(tmp);
      count++;
   }
   return count;
}

/****************************************************************************/
/* Fill in the values in the adjancy matrix from the adjacency list         */
/* this will give an adjacency matrix:     a       b       c                */
/*                                    -------------------------             */
/*                                    a |  0       3       2                */
/*                                    b |  3       0       7                */
/*                                    c |  2       7       0                */
/****************************************************************************/
static void cre_adjmat() {

   int i,j, nnodes;
   noderef pnode, pedge;

   nnodes = b_nsize(G);
   if (nnodes > MAXNOD) printf("\n *** System failure - too many nodes! ");
   else {
      for (i=0; i<nnodes; i++) for (j=0; j<nnodes; j++) adjmat[i][j] = INFIN;
      pnode = G;
      for (i=0; i<nnodes; i++) {
         pedge = get_edges(pnode);
         while (!is_empty(pedge)) {
            j = get_pos(pedge);
            adjmat[i][j] = get_ninfo(pedge);
            pedge = etail(pedge);
            }
         pnode = ntail(pnode);
         }
     }
}

/****************************************************************************/
/* DISPLAY the adjacency matrix                                             */
/****************************************************************************/

static void b_pline() {
   int i;
   printf("\n ----"); for (i=0; i<b_nsize(G); i++) printf("----");
   }

static void b_mhead(char * header) {

   noderef pnode;

   b_pline(); printf("\n %s", header); b_pline();
   printf("\n     ");
   pnode = G;
   while (!is_empty(pnode)) {
      printf("  %c ", get_nname(pnode));
      pnode = ntail(pnode);
      }
   b_pline();
   }

static void b_mdisp(int m[MAXNOD][MAXNOD], char * header) {

   int i,j, nnodes;
   noderef pnode;

   b_mhead(header);
   nnodes = b_nsize(G); pnode = G;
   for (i=0; i<nnodes; i++) {
      printf("\n "); printf(" %c: ", get_nname(pnode));
      for (j=0; j<nnodes; j++) 
         if (m[i][j] == INFIN) printf("  * ");
         else printf(" %2d ", m[i][j]);
      pnode = ntail(pnode);
      }
   b_pline();
   printf("\n ");
  }

/****************************************************************************/
/* GRAPH ALGORITHMS                                                         */
/****************************************************************************/
//Utility function
int minDistance(int D[], int sptBinary[], int size){
   int min = INFIN;  
   int index;

   for(int v = 0; v < size; v++){
      if(sptBinary[v] == 0 && D[v] <= min){
         min = D[v];
         index = v;
      }
   }
   return index;   
}
/****************************************************************************/
/* Dijkstra-SPT                                                             */
/****************************************************************************/
static void b_dispSPT(){
   int size = b_nsize(G);
   noderef temp = G;
   
   //Header
   printf("\n ----");
   for(int i = 0; i < size; i++){
      printf("----");
   }
   printf("\n Dijkstra + SPT\n ----");
   for(int i = 0; i < size; i++){
      printf("----");
   }
   //List nodes
   printf("\n    ");
   for(int i = 0; i < size; i++){
      printf("   %c", get_nname(temp));
      temp = ntail(temp);
   }
   printf("\n ----");
   for(int i = 0; i < size; i++){
      printf("----");
   }
   //D
   printf("\n  D:");
   for(int i = 0; i < size; i++){
      if(D[i] == INFIN || D[i] == 0)
         printf("   *");
      else
         printf("   %d",D[i]);
   }
   //E
   printf("\n  E:");
   for(int i = 0; i < size; i++){
      printf("   %c",E[i]);
   }
   //L
   printf("\n  L:");
   for(int i = 0; i < size; i++){
      if(L[i] == INFIN || L[i] == 0)
         printf("   *");
      else
         printf("   %d",L[i]);
   }
   printf("\n ----");
   for(int i = 0; i < size; i++){
      printf("----");
   }
}

static void b_Dijkstra(char a){
   cre_adjmat();
   int size = b_nsize(G);
   int sptSet[size];

   noderef src = b_findn(a,G);
   noderef tmp = G;

   for(int i = 0; i < size; i++){
      D[i] = INFIN;
      if(i == get_pos(src)){
         E[i] = '*';
      }else{
         E[i] = a;
      }
      L[i] = INFIN;
      sptSet[i] = 0;
   }

   D[get_pos(src)] = 0;
   L[get_pos(src)] = 0;

   for(int i = 0; i < size - 1; i++){
      //Min distance
      int min = minDistance(D,sptSet,size);
      tmp = G;

      //Find corresponding node from index
      for(int j = 0; j < size; j++){
         if(j == min)  
            break;
         else
            tmp = ntail(tmp);
      }
      
      //Mark picked vertex as processed
      sptSet[min] = 1;

      for(int j = 0; j < size; j++){
         if(sptSet[j] == 0 && adjmat[min][j] && D[min] != INFIN && D[min] + adjmat[min][j] < D[j]){
            D[j] = D[min] + adjmat[min][j];
            if(j != get_pos(src)){
               E[j] = get_nname(tmp);
            }
            L[j] = adjmat[min][j];
         } 
      }
   }

   b_dispSPT(); 
}

/****************************************************************************/
/* Floyd                                                                    */
/****************************************************************************/
//Used in b_Floyd and in b_Warshall
static void floydWarshall(){
   cre_adjmat();
   for(int i = 0; i < b_nsize(G); i++){
      for(int j = 0; j < b_nsize(G); j++){ 
         if(i == j){
            Floyd[i][j] = 0;
         }else{
            Floyd[i][j] = adjmat[i][j];
         } 
      }
   }
   for(int k = 0; k < b_nsize(G); k++){ 
      for(int i = 0; i < b_nsize(G); i++){
         for(int j = 0; j < b_nsize(G); j++){
            if(Floyd[i][j] <= Floyd[i][k] + Floyd[k][j]){
               Floyd[i][j] = Floyd[i][j];   
            }else{
               Floyd[i][j] = Floyd[i][k] + Floyd[k][j];
            }
         } 
      }
   }
}

static void b_Floyd(){
   floydWarshall();
   b_mdisp(Floyd, "Floyd");
}

/****************************************************************************/
/* Warshall                                                                 */
/****************************************************************************/
static void b_Warshall(){
   floydWarshall();
   printf("\n\t");
   for(int i = 0; i < b_nsize(G); i++){
      for(int j = 0; j < b_nsize(G); j++){
         if(Floyd[i][j] == INFIN){
            Warshall[i][j] = 0;
         }else{
            Warshall[i][j] = 1;
         } 
      } 
      printf("\n\t"); 
   }
   b_mdisp(Warshall, "Warshall 1 = YES, 0 = NO");
}

/****************************************************************************/
/* Prim                                                                     */
/****************************************************************************/
static void b_dispMST(){ 
   int size = b_nsize(G);
   noderef temp = G;
   //Print N values
   printf("\nPrim + MST\nN: ");//Main header + Header
   for(int i = 0; i < size; i++){
      printf("%7c",get_nname(temp));
      temp = ntail(temp);
   }
   //Print LC values
   printf("\n\nlc:");//Header
   for(int i = 0; i < size; i++){
      if(lowcost[i] == INFIN || lowcost[i] == 0){
         printf("%7s","*");
      }else{
         printf("%7d",lowcost[i]);
      }
   }
   //Print CL values
   printf("\ncl:"); //Header
   for(int i = 0; i < size; i++){
      printf("%7c",closest[i]);
   }
}

static void b_sumMST(){
   int c = 0; //Counting variable = 0
   int size = b_nsize(G); //The size of the graph
   //Loop trhough the nodes
   for(int i = 0; i < size; i++){
      if(lowcost[i] != INFIN){ //If not INFINITE cost in lowcost arrray then
         c += lowcost[i]; //Add to count
      }
   }
   printf("\n\nMST sum is: %d\n",c); //Print the sum
}

static void b_Prim(char a){
   cre_adjmat();
   int size = b_nsize(G);
   int index;
   int visited[size];
   //Look for the source node specified in argument a
   noderef src = b_findn(a,G);
   noderef tmp = NULLREF;
   //Initialize values for the 3 arrays
   for(int i = 0; i < size; i++){
      lowcost[i]  =  INFIN;
      closest[i]  =  a;
      visited[i]  =  0;
   }
   //The source to source distance is 0
   index = get_pos(src); //Source is visited
   lowcost[index] = 0; //0 distance
   closest[index] = '*'; //Insert a chracter
   for(int i = 0; i < size - 1; i++){
      //Pick minimum key vertex
      int distance = minDistance(lowcost, visited,size);
      tmp = G;
      //Find correct node from index u
      for(int j = 0; j < size; j++){
         if(j == distance){
            break;
         }else{
            tmp = ntail(tmp);
         }
      }
      //Mark picked vertex as visited
      visited[distance] = 1;
      for(int j = 0; j < size; j++){
         if(adjmat[distance][j] && visited[j] == 0 && adjmat[distance][j] < lowcost[j]){
            lowcost[j] = adjmat[distance][j];
            closest[j] = get_nname(tmp);
         }
      }
   }
   b_dispMST(); //Display the MST (minimum spanning tree)
   b_sumMST(); //Display the MST sum
}   


/****************************************************************************/
/* public operations on the node                                            */
/****************************************************************************/
void be_display_adjlist()      { b_ndisp(G); }
void be_display_adjmatrix()
   { cre_adjmat(); b_mdisp(adjmat, "Adjacency Matrix"); }

void be_addnode(char c)  { G = b_addn(c, G); }
void be_remnode(char c)  { G = b_remn(c, G); b_remalle(c, G); }

void be_addedge(char cs, char cd, int v) {
   set_edges(b_findn(cs, G), b_adde(cd, v, get_edges(b_findn(cs, G))));
   }

void be_remedge(char cs, char cd) {
   set_edges(b_findn(cs, G), b_reme(cd, get_edges(b_findn(cs, G))));
   }

int be_is_nmember(char c) { return !is_empty(b_findn(c, G)); }

int be_is_emember(char cs, char cd) {
   return be_is_nmember(cs) &&
          !is_empty(b_finde(cd, get_edges(b_findn(cs, G))));
   }

int be_nsize() { return b_nsize(G); }
int be_esize() { return b_esize(G); }

/****************************************************************************/
/* GRAPH ALGORITHMS                                                         */
/****************************************************************************/
void be_Dijkstra(char node)  { b_Dijkstra(node);  }
void be_Floyd()              { b_Floyd();         }
void be_Warshall()           { b_Warshall();      }
void be_Prim(char node)      { b_Prim(node);      }
/****************************************************************************/
/* end of basic functions                                                   */
/****************************************************************************/



