#Link commands
graph: Dmenu.o uimenu.o fegraph.o begraph.o
	gcc -Wall -lm -o graph Dmenu.o uimenu.o fegraph.o begraph.o

#Compilation commands
Dmenu.o: Dmenu.c
	gcc -Wall -c Dmenu.c
uimenu.o: uimenu.c
	gcc -Wall -c uimenu.c
fegraph.o: fegraph.c
	gcc -Wall -c fegraph.c
begraph.o: begraph.c
	gcc -Wall -c begraph.c

#Misc comands
run: graph
	./graph

clean:
	rm -f *.o graph

testcript1: graph
	./graph < script1 > script1.out